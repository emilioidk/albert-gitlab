
# -*- coding: utf-8 -*-

"""GitLab extension.

Access your GitLab repositories and clone them to your configured path.
Search will occurr without having to type the trigger. For example, \
if you have a repository called "awesome-albert" then when you type \
awes in your Albert the option will likely show.

TODO: Re-implement config
TODO: Allow inputting Access Token in a secure terminal
TODO: Don't allow setting access token through environment variable, use password manager instead
TODO: Define usage
TODO: caching all projects per session
TODO: List and config user groups.
TODO: Add docs for functions
TODO: Add README with gifs
TODO: Implement configuration profiles
TODO: Remove python-gitlab dependency by building the part of the API we use
TODO: Show open MRs per project
TODO: Break functionalities into modules
"""

from albert import *
import gitlab
import os
import subprocess
from pydoc import importfile
from time import sleep

module_path = os.path.realpath(os.path.dirname(__file__))
# config = importfile(module_path + "/config.py")

md_iid = '0.5'
md_version = '1.0'
md_name = 'GitLab'
md_description = 'Search through your GitLab repos, clone them and stuff.'
__doc__ = 'Search through your GitLab repos, clone them and stuff.'
md_license = 'MIT'
md_url = 'https://gitlab.com/emilioidk/albert-gitlab'
md_maintainers = 'Emilio Martin Lundgaard Lopez'
md_bin_dependencies = ['code', 'git']
md_lib_dependencies = ['python-gitlab']

icon_path = module_path+"/logo.svg"
gray_icon_path = module_path+"/logo-gray.svg"

# Variables
gitlab_access_token = None
gitlab_con = None


class Plugin(QueryHandler):
    def id(self):
        return 'GitLab'

    def name(self):
        return md_name

    def description(self):
        return __doc__

    def defaultTrigger(self):
        return ''

    def initialize(self):
        debug('Initialized GitLab plugin')

    def finalize(self):
        debug('Finalized GitLab plugin')

    def synopsis(self):
        global gitlab_access_token

        if gitlab_access_token is None:
            return 'gl <access-token>'

        return 'gl <search-term>'

    def handleQuery(self, query):
        global gitlab_access_token
        global gitlab_con

        if gitlab_access_token is None:
            # Access Token
            # Force user to provide an access token
            item = Item(
                id="provide-gitlab-access-token",
                text="Set GitLab Access Token",
                subtext="gl <access token>",
                icon=[icon_path],
                actions=[
                    Action(
                        id="gitlab-access-token",
                        text="Provide GitLab Access Token",
                        callable=lambda: set_access_token_connect(query.string)
                    )
                ]
            )
            query.add([item])
            return
        else:
            # Search
            for number in range(35):
                sleep(0.01)
                if not query.isValid:
                    return

            # GitLab requires at least 3 characters for searching
            if len(query.string) < 3:
                return

            # At this point we already have a key, and enough characters... lets get to work
            projects = gitlab_con.projects.list(
                membership=True,
                search=query.string,
                limit=5,
                order_by='last_activity_at',
                sort='desc'
            )
            query.add([
                Item(
                    id="gitlab_project_{}".format(project.id),
                    text=project.name,
                    subtext='{}'.format(
                        project.description if project.description is not None else ''),
                    icon=[gray_icon_path if project.archived else icon_path],
                    completion="gl {}".format(project.path),
                    actions=[
                        Action(
                            id="url",
                            text="Go to project's website",
                            callable=make_open_url_function(project)
                        ),
                        Action(
                            id="code",
                            text="Open in VS Code",
                            callable=make_open_in_code_function(project)
                        ),
                        Action(
                            id="copy_url",
                            text="Copy project's URL to clipboard",
                            callable=make_copy_to_clipboard_function(project)
                        ),
                    ],
                ) for project in projects
            ])


def open_in_code(project_path, project_ssh_url_to_repo):
    # GITHOME = config.get('GITHOME')
    GITHOME = "/home/emilio/git"
    target_dir = "{}/{}".format(GITHOME, project_path)
    if (not os.path.isdir(target_dir)):
        os.chdir(GITHOME)
        os.system("git clone {}".format(project_ssh_url_to_repo))
    subprocess.call(["code", target_dir])


def make_open_url_function(project):
    return lambda: openUrl(url=project.web_url)


def make_open_in_code_function(project):
    return lambda: open_in_code(project.path, project.ssh_url_to_repo)


def make_copy_to_clipboard_function(project):
    return lambda: setClipboardText(text=project.web_url)


def set_access_token_connect(access_token):
    """Sets the global Access Token and creates the connection to GitLab

    Parameters:
    access_token (string): access token to use when communicating with GitLab
    """
    global gitlab_access_token
    global gitlab_con

    gitlab_access_token = access_token
    gitlab_con = gitlab.Gitlab(private_token=access_token)
    gitlab_con.auth()
