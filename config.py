# -*- coding: utf-8 -*-

from albert import *

import os
from pathlib import Path
import sqlite3

### CONFIG ###

# Defaults
defaults = {
    "GITHOME": "/home/{}/git".format(os.getlogin())
}

# Constants
CONFIG_FILE_PATH = "{}/albert-gitlab".format(configLocation())
CONFIG_FILE = "{}/config.db".format(CONFIG_FILE_PATH)

# Variables
conn = None


def initialize():
    global conn

    # Ensure parent folder exists
    Path(CONFIG_FILE_PATH).mkdir(parents=True, exist_ok=True)

    # Create connection
    conn = sqlite3.connect(CONFIG_FILE, check_same_thread=False)

    # Ensure entries table exists
    sql_create_entries_table = """
    CREATE TABLE IF NOT EXISTS entries (
        id integer PRIMARY KEY,
        key text UNIQUE,
        value text
    );
    """
    conn.execute(sql_create_entries_table)
    conn.commit()


def set(key, value):
    global conn

    sql_upsert_key_value = """
    INSERT INTO entries(key,value) VALUES('{}','{}')
    ON CONFLICT(key) DO UPDATE SET value=excluded.value;
    """.format(key, value)
    conn.execute(sql_upsert_key_value)
    conn.commit()


def get(key):
    global conn

    cursor = conn.cursor()
    cursor.execute("SELECT value FROM entries WHERE key='{}'".format(key))
    entry = cursor.fetchone()
    if entry is not None:
        return entry[0]
    if key in defaults:
        return defaults[key]
    raise Exception("Unable to find value for key {}".format(key))
